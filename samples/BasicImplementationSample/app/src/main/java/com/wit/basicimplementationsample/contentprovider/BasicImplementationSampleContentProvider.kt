package com.wit.basicimplementationsample.contentprovider

import android.content.Context
import com.wit.basicimplementationsample.contentprovider.contract.UserContract
import com.wit.databaselibrary.contentprovider.SimpleContentProvider
import com.wit.databaselibrary.contentprovider.contract.Contract

class BasicImplementationSampleContentProvider : SimpleContentProvider(CONTRACTS) {
	companion object {
		val AUTHORITY: String = BasicImplementationSampleContentProvider::class.java.name
		private const val DATABASE_NAME = "basic-implementation-sample.db"
		private const val DATABASE_VERSION = 1
		private val CONTRACTS = HashSet<Contract<*>>()

		init {
			CONTRACTS.add(UserContract)
		}
	}

	override val authority: String
		get() = AUTHORITY

	override fun createDatabaseHelper(): SimpleDatabaseHelper {
		val context = this.context!!
		val databaseHelper = DatabaseHelper(context)

		return databaseHelper
	}

	class DatabaseHelper(context: Context) :
			SimpleDatabaseHelper(context, DATABASE_NAME, DATABASE_VERSION, CONTRACTS)
}