package com.wit.basicimplementationsample.model

import com.wit.basicimplementationsample.contentprovider.contract.UserContract
import com.wit.databaselibrary.annotation.Column
import com.wit.databaselibrary.annotation.Table
import com.wit.databaselibrary.model.DatabaseObject

@Table(tableName = UserContract.Columns.TABLE_NAME)
class User( //
		@Column(name = UserContract.Columns.AGE) var age: Int?, //
		@Column(name = UserContract.Columns.FIRST_NAME) var firstName: String?) : DatabaseObject()