package com.wit.basicimplementationsample

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import com.wit.basicimplementationsample.model.User
import com.wit.basicimplementationsample.service.UserManager

class UpdateFragment : android.support.v4.app.Fragment() {
	private val viewHolder = ViewHolder()

	override fun onCreateView(layoutInflater: LayoutInflater, container: ViewGroup?,
			savedInstanceState: Bundle?): View? {
		val view = layoutInflater.inflate(R.layout.update, container, false)

		this.viewHolder.ageEditText = view.findViewById<View>(R.id.age) as EditText
		this.viewHolder.firstNameEditText = view.findViewById<View>(R.id.first_name) as EditText
		this.viewHolder.idEditText = view.findViewById<View>(R.id.id) as EditText
		this.viewHolder.updateButton = view.findViewById<View>(R.id.update) as Button

		val updateOnClickListener = UpdateOnClickListener(this.viewHolder)

		this.viewHolder.updateButton!!.setOnClickListener(updateOnClickListener)

		return view
	}

	private class UpdateOnClickListener(private val viewHolder: ViewHolder) : View.OnClickListener {
		override fun onClick(view: View) {
			val idString = this.viewHolder.idEditText!!.text.toString()
			val ageString = this.viewHolder.ageEditText!!.text.toString()
			val firstName = this.viewHolder.firstNameEditText!!.text.toString()

			if (idString.isNotEmpty() && ageString.isNotEmpty() && firstName.isNotEmpty()) {
				val context = view.context
				val updateAsyncTask = UpdateAsyncTask(context, this.viewHolder)

				updateAsyncTask.execute()
			} else {
				val context = view.context
				val toast =
						Toast.makeText(context, "Please specify a valid ID, age, and first name.",
								Toast.LENGTH_LONG)

				toast.show()
			}
		}

		@SuppressLint("StaticFieldLeak")
		private class UpdateAsyncTask(private val context: Context,
				private val viewHolder: ViewHolder) : AsyncTask<Void, Void, User>() {
			private var idString: String? = null
			private var ageString: String? = null
			private var firstName: String? = null

			override fun onPreExecute() {
				super.onPreExecute()

				this.idString = this.viewHolder.idEditText!!.text.toString()
				this.ageString = this.viewHolder.ageEditText!!.text.toString()
				this.firstName = this.viewHolder.firstNameEditText!!.text.toString()
			}

			override fun doInBackground(vararg params: Void): User? {
				val userManager = UserManager.getInstance(this.context)!!
				val idString = this.idString!!
				val id = java.lang.Long.parseLong(idString)
				val existingUser = userManager[id]
				val savedUser: User?

				if (existingUser == null) {
					savedUser = null
				} else {
					val ageString = this.ageString!!
					val age = Integer.parseInt(ageString)

					existingUser.age = age
					existingUser.firstName = this.firstName

					savedUser = userManager.save(existingUser)
				}

				return savedUser
			}

			override fun onPostExecute(user: User?) {
				super.onPostExecute(user)

				val message = if (user == null) {
					String.format("No user with ID \"%1\$s\" found.", this.idString)
				} else {
					"Updated user: $user"
				}

				val toast = Toast.makeText(this.context, message, Toast.LENGTH_LONG)

				toast.show()
			}
		}
	}

	private class ViewHolder {
		var ageEditText: EditText? = null
		var firstNameEditText: EditText? = null
		var idEditText: EditText? = null
		var updateButton: Button? = null
	}
}