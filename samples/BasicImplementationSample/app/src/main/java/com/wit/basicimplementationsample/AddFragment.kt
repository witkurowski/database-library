package com.wit.basicimplementationsample

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import com.wit.basicimplementationsample.model.User
import com.wit.basicimplementationsample.service.UserManager

class AddFragment : android.support.v4.app.Fragment() {
	private val viewHolder = ViewHolder()

	override fun onCreateView(layoutInflater: LayoutInflater, container: ViewGroup?,
			savedInstanceState: Bundle?): View? {
		val view = layoutInflater.inflate(R.layout.add, container, false)

		this.viewHolder.addButton = view.findViewById<View>(R.id.add) as Button
		this.viewHolder.ageEditText = view.findViewById<View>(R.id.age) as EditText
		this.viewHolder.firstNameEditText = view.findViewById<View>(R.id.first_name) as EditText

		val addOnClickListener = AddOnClickListener(this.viewHolder)

		this.viewHolder.addButton!!.setOnClickListener(addOnClickListener)

		return view
	}

	private class AddOnClickListener(private val viewHolder: ViewHolder) : View.OnClickListener {
		override fun onClick(view: View) {
			val ageString = this.viewHolder.ageEditText!!.text.toString()
			val firstName = this.viewHolder.firstNameEditText!!.text.toString()

			if (ageString.isNotEmpty() && firstName.isNotEmpty()) {
				val context = view.context
				val age = Integer.parseInt(ageString)
				val newUser = User(age, firstName)
				val addAsyncTask = AddAsyncTask(context, newUser, this.viewHolder)

				addAsyncTask.execute()
			} else {
				val context = view.context
				val toast = Toast.makeText(context, "Please specify a valid age and first name.",
						Toast.LENGTH_LONG)

				toast.show()
			}
		}

		@SuppressLint("StaticFieldLeak")
		private class AddAsyncTask(private val context: Context, private val newUser: User,
				private val viewHolder: ViewHolder) : AsyncTask<Void, Void, User>() {
			override fun doInBackground(vararg params: Void): User? {
				val userManager = UserManager.getInstance(this.context)!!

				return userManager.save(newUser)
			}

			override fun onPostExecute(user: User) {
				super.onPostExecute(user)

				val toast = Toast.makeText(this.context, "Created user: $user", Toast.LENGTH_LONG)

				toast.show()

				this.viewHolder.ageEditText!!.text = null
				this.viewHolder.firstNameEditText!!.text = null
			}
		}
	}

	private class ViewHolder {
		var addButton: Button? = null
		var ageEditText: EditText? = null
		var firstNameEditText: EditText? = null
	}
}