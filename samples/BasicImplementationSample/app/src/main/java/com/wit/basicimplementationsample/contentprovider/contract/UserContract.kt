package com.wit.basicimplementationsample.contentprovider.contract

import com.wit.basicimplementationsample.model.User
import com.wit.databaselibrary.contentprovider.contract.Contract

object UserContract : Contract<User>(User::class.java) {
	class Columns : Contract.Columns() {
		companion object {
			const val TABLE_NAME = "USER"
			const val AGE = "age"
			const val FIRST_NAME = "first_name"
		}
	}
}