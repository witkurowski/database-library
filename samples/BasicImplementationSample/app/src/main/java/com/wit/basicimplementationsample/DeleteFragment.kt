package com.wit.basicimplementationsample

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.wit.basicimplementationsample.service.UserManager

class DeleteFragment : android.support.v4.app.Fragment() {
	private val viewHolder = ViewHolder()

	override fun onCreateView(layoutInflater: LayoutInflater, container: ViewGroup?,
			savedInstanceState: Bundle?): View? {
		val view = layoutInflater.inflate(R.layout.delete, container, false)

		this.viewHolder.deleteAllButton = view.findViewById<View>(R.id.delete_all) as Button
		this.viewHolder.deleteButton = view.findViewById<View>(R.id.delete) as Button
		this.viewHolder.idEditText = view.findViewById<View>(R.id.id) as EditText

		val deleteOnClickListener = DeleteOnClickListener(this.viewHolder)

		this.viewHolder.deleteButton!!.setOnClickListener(deleteOnClickListener)

		val deleteAllOnClickListener = DeleteAllOnClickListener()

		this.viewHolder.deleteAllButton!!.setOnClickListener(deleteAllOnClickListener)

		return view
	}

	private class DeleteAllOnClickListener : View.OnClickListener {
		override fun onClick(view: View) {
			val context = view.context
			val deleteAllAsyncTask = DeleteAllAsyncTask(context)

			deleteAllAsyncTask.execute()
		}

		@SuppressLint("StaticFieldLeak")
		private class DeleteAllAsyncTask(private val context: Context) :
				AsyncTask<Void, Void, Int>() {
			override fun doInBackground(vararg params: Void): Int? {
				val userManager = UserManager.getInstance(this.context)!!

				return userManager.delete()
			}

			override fun onPostExecute(numberOfDeletions: Int?) {
				super.onPostExecute(numberOfDeletions)

				val message =
						String.format("%1\$s user(s) successfully deleted.", numberOfDeletions)
				val toast = Toast.makeText(this.context, message, Toast.LENGTH_LONG)

				toast.show()
			}
		}
	}

	private class DeleteOnClickListener(private val viewHolder: ViewHolder) : View.OnClickListener {
		override fun onClick(view: View) {
			val idString = this.viewHolder.idEditText!!.text.toString()

			if (idString.isNotEmpty()) {
				val context = view.context
				val deleteAsyncTask = DeleteAsyncTask(context, this.viewHolder)

				deleteAsyncTask.execute()
			} else {
				val context = view.context
				val toast = Toast.makeText(context, "Please specify a valid ID.", Toast.LENGTH_LONG)

				toast.show()
			}
		}

		@SuppressLint("StaticFieldLeak")
		private class DeleteAsyncTask(private val context: Context,
				private val viewHolder: ViewHolder) : AsyncTask<Void, Void, Int>() {
			private var idString: String? = null

			override fun onPreExecute() {
				super.onPreExecute()

				this.idString = this.viewHolder.idEditText!!.text.toString()
			}

			override fun doInBackground(vararg params: Void): Int? {
				val userManager = UserManager.getInstance(this.context)!!
				val idString = this.idString!!
				val id = java.lang.Long.parseLong(idString)
				val existingUser = userManager[id]
				val numberOfDeletions = if (existingUser == null) {
					0
				} else {
					userManager.delete(existingUser)
				}

				return numberOfDeletions
			}

			override fun onPostExecute(numberOfDeletions: Int?) {
				super.onPostExecute(numberOfDeletions)

				val message = if (numberOfDeletions == 0) {
					String.format("No user with ID \"%1\$s\" found.", this.idString)
				} else {
					String.format("User with ID \"%1\$s\" successfully deleted.", this.idString)
				}

				val toast = Toast.makeText(this.context, message, Toast.LENGTH_LONG)

				toast.show()

				this.viewHolder.idEditText!!.text = null
			}
		}
	}

	private class ViewHolder {
		var deleteAllButton: Button? = null
		var deleteButton: Button? = null
		var idEditText: EditText? = null
	}
}