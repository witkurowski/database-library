package com.wit.basicimplementationsample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTabHost
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.FrameLayout
import android.widget.TabWidget

class MainActivity : AppCompatActivity() {
	private val viewHolder = ViewHolder()

	private class ViewHolder {
		var tabContentFrameLayout: FrameLayout? = null
		var fragmentTabHost: FragmentTabHost? = null
		var tabWidget: TabWidget? = null
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.main)

		this.setupToolbar()
		this.setupViewHolder()
		this.setupTabs()
	}

	private fun setupTab(tag: String, indicatorLabelResourceId: Int,
			fragmentClass: Class<out Fragment>) {
		val tabSpec = this.viewHolder.fragmentTabHost!!.newTabSpec(tag)
		val indicatorLabel = this.getString(indicatorLabelResourceId)

		tabSpec.setIndicator(indicatorLabel)

		this.viewHolder.fragmentTabHost!!.addTab(tabSpec, fragmentClass, null)
	}

	private fun setupTabs() {
		val supportFragmentManager = this.supportFragmentManager

		this.viewHolder.fragmentTabHost!!.setup(this, supportFragmentManager,
				android.R.id.tabcontent)

		this.setupTab("add", R.string.add, AddFragment::class.java)
		this.setupTab("get", R.string.get, GetFragment::class.java)
		this.setupTab("update", R.string.update, UpdateFragment::class.java)
		this.setupTab("delete", R.string.delete, DeleteFragment::class.java)
	}

	private fun setupToolbar() {
		val toolbar = this.findViewById(R.id.toolbar) as Toolbar

		this.setSupportActionBar(toolbar)
	}

	private fun setupViewHolder() {
		this.viewHolder.tabContentFrameLayout = this.findViewById(android.R.id.tabcontent)
		this.viewHolder.fragmentTabHost = this.findViewById(android.R.id.tabhost)
		this.viewHolder.tabWidget = this.findViewById(android.R.id.tabs)
	}
}