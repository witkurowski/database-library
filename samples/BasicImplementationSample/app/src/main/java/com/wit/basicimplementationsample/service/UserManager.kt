package com.wit.basicimplementationsample.service

import android.content.Context
import com.wit.basicimplementationsample.contentprovider.BasicImplementationSampleContentProvider
import com.wit.basicimplementationsample.contentprovider.contract.UserContract
import com.wit.basicimplementationsample.model.User
import com.wit.databaselibrary.service.Manager

class UserManager private constructor(context: Context) :
		Manager<User>(context, UserContract, User::class.java) {
	override val authority: String
		get() = BasicImplementationSampleContentProvider.AUTHORITY

	companion object {
		private var userManager: UserManager? = null

		@Synchronized
		fun getInstance(context: Context): UserManager? {
			if (UserManager.userManager == null) {
				UserManager.userManager = UserManager(context)
			}

			return UserManager.userManager
		}
	}
}