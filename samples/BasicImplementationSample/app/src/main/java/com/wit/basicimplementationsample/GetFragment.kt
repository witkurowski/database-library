package com.wit.basicimplementationsample

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import com.wit.basicimplementationsample.model.User
import com.wit.basicimplementationsample.service.UserManager

class GetFragment : android.support.v4.app.Fragment() {
	private val viewHolder = ViewHolder()

	override fun onCreateView(layoutInflater: LayoutInflater, container: ViewGroup?,
			savedInstanceState: Bundle?): View? {
		val view = layoutInflater.inflate(R.layout.get, container, false)

		this.viewHolder.getButton = view.findViewById<View>(R.id.get) as Button
		this.viewHolder.getAllButton = view.findViewById<View>(R.id.get_all) as Button
		this.viewHolder.idEditText = view.findViewById<View>(R.id.id) as EditText

		val getOnClickListener = GetOnClickListener(this.viewHolder)

		this.viewHolder.getButton!!.setOnClickListener(getOnClickListener)

		val getAllOnClickListener = GetAllOnClickListener()

		this.viewHolder.getAllButton!!.setOnClickListener(getAllOnClickListener)

		return view
	}

	private class GetAllOnClickListener : View.OnClickListener {
		override fun onClick(view: View) {
			val context = view.context
			val getAllAsyncTask = GetAllAsyncTask(context)

			getAllAsyncTask.execute()
		}

		@SuppressLint("StaticFieldLeak")
		private class GetAllAsyncTask(private val context: Context) :
				AsyncTask<Void, Void, List<User>>() {
			override fun doInBackground(vararg params: Void): List<User> {
				val userManager = UserManager.getInstance(this.context)!!

				return userManager.get()
			}

			override fun onPostExecute(users: List<User>) {
				super.onPostExecute(users)

				val message = if (users.isEmpty()) {
					"No users found."
				} else {
					String.format("Retrieved %1\$d user(s): %2\$s", users.size, users)
				}

				val toast = Toast.makeText(this.context, message, Toast.LENGTH_LONG)

				toast.show()
			}
		}
	}

	private class GetOnClickListener(private val viewHolder: ViewHolder) : View.OnClickListener {
		override fun onClick(view: View) {
			val idString = this.viewHolder.idEditText!!.text.toString()

			if (idString.isNotEmpty()) {
				val context = view.context
				val getAsyncTask = GetAsyncTask(context, this.viewHolder)

				getAsyncTask.execute()
			} else {
				val context = view.context
				val toast = Toast.makeText(context, "Please specify a valid ID.", Toast.LENGTH_LONG)

				toast.show()
			}
		}

		@SuppressLint("StaticFieldLeak")
		private class GetAsyncTask(private val context: Context,
				private val viewHolder: ViewHolder) : AsyncTask<Void, Void, User>() {
			private var idString: String? = null

			override fun onPreExecute() {
				super.onPreExecute()

				this.idString = this.viewHolder.idEditText!!.text.toString()
			}

			override fun doInBackground(vararg params: Void): User? {
				val userManager = UserManager.getInstance(this.context)!!
				val idString = this.idString!!
				val id = java.lang.Long.parseLong(idString)

				return userManager[id]
			}

			override fun onPostExecute(user: User?) {
				super.onPostExecute(user)

				val message = if (user == null) {
					String.format("No user with ID \"%1\$s\" found.", this.idString)
				} else {
					"Retrieved user: $user"
				}

				val toast = Toast.makeText(this.context, message, Toast.LENGTH_LONG)

				toast.show()
			}
		}
	}

	private class ViewHolder {
		var getAllButton: Button? = null
		var getButton: Button? = null
		var idEditText: EditText? = null
	}
}