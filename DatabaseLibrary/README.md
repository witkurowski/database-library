Add subclasses of `DatabaseObject` for each model to be stored.  These subclasses should contain an
`@Table` annotation specifying a unique database table name at the top of each model class, as well
as `@Column` annotations specifying column names and types for each field from that model that is
meant to be stored.
```
@Table(tableName = "MY_MODEL")
public class MyModel extends DatabaseObject {
	@Column(columnName = "amount", columnType = ColumnType.INTEGER)
	private String amount;

	@Column(columnName = "category", columnType = ColumnType.ENUM)
	private Category category;

	@Column(columnName = "descrjption", columnType = ColumnType.STRING)
	private String description;

	// ...
}
```

Add contract classes to your project for each model to be stored:
```
public class MyModelContract extends Contract<MyModel> {
	private static final MyModelContract MY_MODEL_CONTRACT = new MyModelContract();

	public static MyModelContract getInstance() {
		return MyModelContract.MY_MODEL_CONTRACT;
	}

	private MyModelContract() {
		super(MyModel.class);
	}
}
```

Add a subclass of `SimpleDatabaseHelper` to your project containing the current database version as
well as the desired database name:
```
public final class MyAppDatabaseHelper extends SimpleDatabaseHelper {
	private static final String DATABASE_NAME = "MyApp";
	private static final int DATABASE_VERSION = 1;

	public MyAppDatabaseHelper(final Context context, final Set<Contract> contracts) {
		super(context, MyAppDatabaseHelper.DATABASE_NAME,
				MyAppDatabaseHelper.DATABASE_VERSION, contracts);
	}
}
```

Add a subclass of `SimpleContentProvider` to your project passing in all the `Contract` subclass
singletons defined in your project through the constructor.
```
public final class MyAppContentProvider extends SimpleContentProvider {
	public static final String AUTHORITY = MyAppContentProvider.class.getName();
	private static final Set<Contract> CONTRACTS = new HashSet<>();

	static {
		MyAppContentProvider.CONTRACTS.add(MyModelContract.getInstance());
	}

	public MyAppContentProvider() {
		super(MyAppContentProvider.CONTRACTS);
	}

	@Override
	protected SimpleDatabaseHelper createDatabaseHelper() {
		final Context context = this.getContext();
		final MyAppDatabaseHelper myAppDatabaseHelper = new MyAppDatabaseHelper(context,
				MyAppContentProvider.CONTRACTS);

		return myAppDatabaseHelper;
	}

	@Override
	protected String getAuthority() {
		return MyAppContentProvider.AUTHORITY;
	}
}
```

Add subclasses of `Manager` for each model to be stored, which will handle all basic CRUD operations
and can easily be extended with more powerful operations:
```
public class MyModelManager extends Manager<MyModel> {
	private static final MyModelContract MY_MODEL_CONTRACT = MyModelContract.getInstance();
	private static MyModelManager myModelManager;

	public static synchronized MyModelManager getInstance(final Context context) {
		if (MyModelManager.myModelManager == null) {
			MyModelManager.myModelManager = new MyModelManager(context);
		}

		return myModelManager;
	}

	private MyModelManager(final Context context) {
		super(context, MyModelManager.MY_MODEL_CONTRACT, MyModel.class);
	}

	@Override
	protected String getAuthority() {
		return MyAppContentProvider.AUTHORITY;
	}
}
```

In the `AndroidManifest.xml` file, add
```
<provider
		android:authorities="<authority-name>"
		android:exported="false"
		android:name="<path-to-content-provider>" />
```
where `<authority-name>` is the name of the authority defined in your subclass of
`SimpleContentProvider` (e.g.,`com.example.myapp.contentprovider.MyAppContentProvider`) and
`<path-to-content-provider>` is the path to, and name of, the content provider
(e.g., `.contentprovider.MyAppContentProvider`).