package com.wit.databaselibrary.annotation

@kotlin.annotation.Retention
@Target(AnnotationTarget.FIELD)
annotation class Column(val name: String)