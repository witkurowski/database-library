package com.wit.databaselibrary.annotation

@kotlin.annotation.Retention
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class Table(val tableName: String)