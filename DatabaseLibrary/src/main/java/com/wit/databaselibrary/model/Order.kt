package com.wit.databaselibrary.model

enum class Order(val keyword: String) {
    ASCENDING("ASC"), DESCENDING("DESC")
}