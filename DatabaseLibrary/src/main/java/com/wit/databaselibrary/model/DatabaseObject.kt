package com.wit.databaselibrary.model

import android.provider.BaseColumns

import com.wit.databaselibrary.annotation.Column
import com.wit.databaselibrary.contentprovider.contract.Contract

open class DatabaseObject(
		/**
		 * The unique identifier for this object.
		 */
		@Column(name = BaseColumns._ID) val id: Long? = null,
		/**
		 * The numerical value used to determine which instance of an object is most up-to-date.
		 */
		@Column(name = Contract.Columns.VERSION) var version: Long? = 1L) {
	/**
	 * Returns whether this object's ID is managed externally.
	 *
	 * @return Whether this object's ID is managed externally.
	 */
	val isIdManagedExternally: Boolean
		get() = false

	/**
	 * Returns whether this object's version number is managed externally.
	 *
	 * @return Whether this object's version number is managed externally.
	 */
	val isVersionManagedExternally: Boolean
		get() = false
}