package com.wit.databaselibrary.service.exception

@Suppress("unused")
class InvalidClassDefinitionException : RuntimeException {
	constructor()

	constructor(cause: Throwable) : super(cause)

	constructor(message: String) : super(message)

	constructor(message: String, cause: Throwable) : super(message, cause)

	constructor(message: String, cause: Throwable,
			enableSuppression: Boolean, writableStackTrace: Boolean) : super(message, cause,
			enableSuppression, writableStackTrace)
}