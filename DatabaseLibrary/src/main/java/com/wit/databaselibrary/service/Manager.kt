package com.wit.databaselibrary.service

import android.content.ContentProviderOperation
import android.content.ContentResolver
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.OperationApplicationException
import android.database.Cursor
import android.database.SQLException
import android.net.Uri
import android.os.RemoteException
import android.provider.BaseColumns
import android.util.Log
import android.util.Pair
import com.wit.databaselibrary.annotation.Column
import com.wit.databaselibrary.contentprovider.StorageModificationException
import com.wit.databaselibrary.contentprovider.contract.Contract
import com.wit.databaselibrary.model.DatabaseObject
import com.wit.databaselibrary.model.Order
import com.wit.databaselibrary.service.exception.InvalidClassDefinitionException
import org.apache.commons.lang3.tuple.Triple
import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.lang.reflect.InvocationTargetException
import java.util.*

@Suppress("unused")
abstract class Manager<T : DatabaseObject> protected constructor(context: Context,
		private val contract: Contract<T>, private val parameterClass: Class<T>) {
	private val contentResolver: ContentResolver = context.contentResolver
	protected val packageName: String = context.packageName

	protected abstract val authority: String

	/**
	 * Returns the number of [DatabaseObject]s that are saved.
	 *
	 * @return The number of [DatabaseObject]s that are saved.
	 */
	val count: Int
		get() {
			val selectionClause: String? = null
			val selectionArgs = emptyList<String>()

			return getCount(selectionClause, selectionArgs)
		}

	/**
	 * Adds and updates the specified objects.
	 *
	 * @param objectsToAdd The objects that need to be added.
	 * @param objectsToUpdate The objects that need to be updated.
	 * @return The latest version of the objects that have been saved or updated.
	 * @throws StorageModificationException An add or update operation failed.
	 */
	@Throws(StorageModificationException::class)
	private fun apply(objectsToAdd: List<T>, objectsToUpdate: List<T>): List<T> {
		val objectsToDelete = emptyList<T>()
		val savedObjects = apply(objectsToAdd, objectsToUpdate, objectsToDelete)

		return savedObjects
	}

	/**
	 * Adds, updates, and deletes the specified objects.
	 *
	 * @param objectsToAdd The objects that need to be added.
	 * @param objectsToUpdate The objects that need to be updated.
	 * @param objectsToDelete The objects that need to be deleted.
	 * @return The latest version of the objects that have been saved or updated.
	 * @throws StorageModificationException An add, update, or delete operation failed.
	 */
	@Throws(StorageModificationException::class)
	private fun apply(objectsToAdd: List<T>, objectsToUpdate: List<T>,
			objectsToDelete: List<T>): List<T> {
		val authority = this.authority
		val contentProviderOperations = ArrayList<ContentProviderOperation>()
		val addedObjectContentProviderOperations = this.processObjectsToAdd(objectsToAdd)

		contentProviderOperations.addAll(addedObjectContentProviderOperations)

		val modifiedObjectContentProviderOperations = this.processObjectsToUpdate(objectsToUpdate)

		contentProviderOperations.addAll(modifiedObjectContentProviderOperations)

		val deletedObjectContentProviderOperations = this.processObjectsToDelete(objectsToDelete)

		contentProviderOperations.addAll(deletedObjectContentProviderOperations)

		val ids = ArrayList<Long>()

		try {
			val contentProviderResults =
					this.contentResolver.applyBatch(authority, contentProviderOperations)

			for (contentProviderResult in contentProviderResults) {
				val uri = contentProviderResult.uri

				if (uri != null) {
					val idString = uri.lastPathSegment
					val id = java.lang.Long.parseLong(idString)

					ids.add(id)
				}
			}
		} catch (remoteException: RemoteException) {
			Log.e(Manager::class.java.simpleName,
					"An error happened while attempting to communicate with a remote provider.",
					remoteException)
		} catch (operationApplicationException: OperationApplicationException) {
			val errorMessage = if (objectsToDelete.isEmpty()) {
				"An add or update operation failed to be applied."
			} else {
				"An add, update, or delete operation failed to be applied."
			}

			throw StorageModificationException(errorMessage, operationApplicationException)
		}

		return get(ids)
	}

	protected fun categorize(existingObjects: Collection<T>,
			newObjects: Collection<T>): Triple<List<T>, List<T>, List<T>> {
		val existingObjectIdsToSources = mutableMapOf<Long, T>()

		for (existingObject in existingObjects) {
			val existingObjectId = existingObject.id!!

			existingObjectIdsToSources[existingObjectId] = existingObject
		}

		val objectsToAdd = ArrayList<T>()
		val objectsToUpdate = ArrayList<T>()
		val objectsNotIncluded = ArrayList<T>()

		for (newObject in newObjects) {
			val newObjectId = newObject.id
			val objectAlreadyExisted = existingObjectIdsToSources.containsKey(newObjectId)

			if (objectAlreadyExisted) {
				val newObjectVersion = newObject.version!!
				val existingObject = existingObjectIdsToSources.remove(newObjectId)!!
				val existingObjectVersion = existingObject.version!!

				if (newObjectVersion > existingObjectVersion) {
					objectsToUpdate.add(newObject)
				}
			} else {
				objectsToAdd.add(newObject)
			}
		}

		objectsNotIncluded.addAll(existingObjectIdsToSources.values)

		return Triple.of(objectsToAdd, objectsToUpdate, objectsNotIncluded)
	}

	/**
	 * Creates an empty instance of the parameter class passed into this [Manager].
	 *
	 * @return An empty instance of the parameter class passed into this [Manager].
	 */
	private fun createNewEmptyInstance(): T {
		@Suppress("UNCHECKED_CAST") val declaredConstructors =
				this.parameterClass.declaredConstructors as Array<Constructor<T>>
		val declaredConstructor = declaredConstructors[0]
		val parameterTypes = declaredConstructor.parameterTypes
		val newObject = try {
			declaredConstructor.newInstance(*arrayOfNulls(parameterTypes.size))
		} catch (exception: InstantiationException) {
			throw IllegalStateException("Unable to create a new instance of DatabaseObject.",
					exception)
		} catch (exception: IllegalAccessException) {
			throw IllegalStateException("Unable to create a new instance of DatabaseObject.",
					exception)
		} catch (exception: InvocationTargetException) {
			throw IllegalStateException("Unable to create a new instance of DatabaseObject.",
					exception)
		}

		return newObject
	}

	/**
	 * Deletes all [DatabaseObject]s associated with this [Manager].
	 *
	 * @return The number of [DatabaseObject]s that were deleted.
	 */
	fun delete(): Int {
		val whereClause: String? = null
		val whereArgs = emptyList<String>()
		val numberOfRowsDeleted = delete(null, whereClause, whereArgs)

		return numberOfRowsDeleted
	}

	/**
	 * Deletes the given [DatabaseObject]s.
	 *
	 * @param objects The [DatabaseObject]s to delete.
	 * @return The number of [DatabaseObject]s that were deleted.
	 * @throws StorageModificationException A delete operation failed.
	 */
	@Throws(StorageModificationException::class)
	fun delete(objects: Collection<T>) {
		val authority = this.authority
		val contentProviderOperations = ArrayList(this.processObjectsToDelete(objects))

		try {
			this.contentResolver.applyBatch(authority, contentProviderOperations)
		} catch (remoteException: RemoteException) {
			Log.e(Manager::class.java.simpleName,
					"An error happened while attempting to communicate with a remote provider.",
					remoteException)
		} catch (operationApplicationException: OperationApplicationException) {
			throw StorageModificationException("A delete operation failed to be applied.",
					operationApplicationException)
		}

	}

	/**
	 * Deletes the given [DatabaseObject].
	 *
	 * @param object The [DatabaseObject] to delete.
	 * @return The number of [DatabaseObject]s that were deleted.
	 */
	fun delete(`object`: T): Int {
		val id = `object`.id!!
		val whereClause = BaseColumns._ID + " = ?"
		val whereArgs = listOf(id.toString())
		val numberOfRowsDeleted = delete(id, whereClause, whereArgs)

		return numberOfRowsDeleted
	}

	/**
	 * Deletes any [DatabaseObject] that satisfy the given where clause and arguments.
	 *
	 * @param whereClause The where clause to use to narrow down the [DatabaseObject]s to
	 * delete.
	 * @param whereArgs The values to replace the question marks with in the where clause.
	 * @return The number of [DatabaseObject]s that were deleted.
	 */
	fun delete(whereClause: String, whereArgs: List<String>): Int {
		val id: Long? = null
		val numberOfRowsDeleted = delete(id, whereClause, whereArgs)

		return numberOfRowsDeleted
	}

	/**
	 * Deletes the given [DatabaseObject] if the current saved state of it satisfies the
	 * given
	 * where clause and arguments.  Note that the object ID matching condition will be added to the
	 * where clause and argument list.
	 *
	 * @param object The [DatabaseObject] to delete, assuming it satisfies the given where
	 * clause and arguments.
	 * @param whereClause The where clause to use to determine whether the [DatabaseObject]
	 * should be deleted.
	 * @param whereArgs The values to replace the question marks with in the where clause.
	 * @return 1 if the specified [DatabaseObject] was deleted, or 0 if it was not deleted.
	 */
	fun delete(`object`: T, whereClause: String, whereArgs: List<String>): Int {
		val id = `object`.id
		val narrowedWhereClause = BaseColumns._ID + " = ? AND " + whereClause
		val narrowedWhereArgs = ArrayList<String>()

		narrowedWhereArgs.add(id.toString())
		narrowedWhereArgs.addAll(whereArgs)

		val numberOfRowsDeleted = delete(id, narrowedWhereClause, narrowedWhereArgs)

		return numberOfRowsDeleted
	}

	/**
	 * Deletes any [DatabaseObject] that satisfy the given where clause and arguments using
	 * the given ID as part of the content URI.
	 *
	 * @param id The ID to use as part of the content URI associated with the delete, if any.
	 * @param whereClause The where clause to use to narrow down the [DatabaseObject]s to
	 * delete.
	 * @param whereArgs The values to replace the question marks with in the where clause.
	 * @return The number of [DatabaseObject]s that were deleted.
	 */
	private fun delete(id: Long?, whereClause: String?, whereArgs: List<String>): Int {
		val authority = this.authority
		val contentUri = if (id == null) {
			this.contract.getContentUri(authority)
		} else {
			this.contract.getContentUri(authority, id)
		}

		val numberOfRowsDeleted =
				contentResolver.delete(contentUri, whereClause, whereArgs.toTypedArray())

		return numberOfRowsDeleted
	}

	protected fun generateContentValues(`object`: T): ContentValues {
		val contentValues = ContentValues()
		val id = `object`.id

		contentValues.put(BaseColumns._ID, id)

		val version = `object`.version

		contentValues.put(Contract.Columns.VERSION, version)

		val declaredFields = this.parameterClass.declaredFields

		for (declaredField in declaredFields) {
			val column = declaredField.getAnnotation(Column::class.java)

			if (column != null) {
				val columnName = column.name

				if (columnName != BaseColumns._ID && columnName != Contract.Columns.VERSION) {
					val type = declaredField.type
					val isEnumConstant = type.isEnum

					if (isEnumConstant) {
						declaredField.isAccessible = true

						val value: String

						try {
							val fieldValue = declaredField.get(`object`) as Enum<*>

							value = fieldValue.name
						} catch (illegalAccessException: IllegalAccessException) {
							val declaredFieldName = declaredField.name
							val message = String.format("Unable to get value of field \"%s$1\".",
									declaredFieldName)

							throw IllegalStateException(message, illegalAccessException)
						}

						contentValues.put(columnName, value)
					} else {
						val name = declaredField.name
						val fieldIsJavaBoolean = type == java.lang.Boolean::class.java
						val getterMethodName = if (fieldIsJavaBoolean) {
							"is" + name.substring(0, 1).toUpperCase() + name.substring(1)
						} else {
							"get" + name.substring(0, 1).toUpperCase() + name.substring(1)
						}

						val declaredGetterMethod = try {
							this.parameterClass.getDeclaredMethod(getterMethodName,
									*arrayOfNulls(0))
						} catch (noSuchMethodException: NoSuchMethodException) {
							throw InvalidClassDefinitionException(
									"Unable to get find method '$getterMethodName()'.",
									noSuchMethodException)
						}

						try {
							val rawValue = declaredGetterMethod.invoke(`object`)

							when (type) {
								Boolean::class.java, java.lang.Boolean::class.java -> {
									val value = when (rawValue) {
										null -> null
										false -> 0
										else -> 1
									}

									contentValues.put(columnName, value)
								}
								Date::class.java -> {
									val dateValue = rawValue as Date?
									val value = dateValue?.time

									contentValues.put(columnName, value)
								}
								Int::class.java, Integer::class.java -> {
									val value = rawValue as Int?

									contentValues.put(columnName, value)
								}
								Long::class.java, java.lang.Long::class.java -> {
									val value = rawValue as Long?

									contentValues.put(columnName, value)
								}
								String::class.java, java.lang.String::class.java -> {
									val value = rawValue as String?

									contentValues.put(columnName, value)
								}
								else -> {
									throw IllegalArgumentException(
											"Found unknown column type \"$type\".")
								}
							}
						} catch (invocationTargetException: InvocationTargetException) {
							throw IllegalStateException(
									"Unable to get value of type '$type' from method '$getterMethodName()'.",
									invocationTargetException)
						} catch (illegalAccessException: IllegalAccessException) {
							throw InvalidClassDefinitionException(
									"Unable to get value of type '$type' from method '$getterMethodName()'.",
									illegalAccessException)
						}
					}
				}
			}
		}

		return contentValues
	}

	fun get(): List<T> {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val projection = this.contract.getColumnNames()
		val cursor =
				this.contentResolver.query(contentUri, projection.toTypedArray(), null, null, null)
		val objects: List<T>

		if (cursor == null) {
			objects = emptyList()
		} else {
			objects = this.getAll(cursor)

			cursor.close()
		}

		return objects
	}

	protected operator fun get(cursor: Cursor?): T {
		val databaseObject = this.createNewEmptyInstance()

		this.populate(databaseObject, cursor)

		return databaseObject
	}

	private operator fun get(ids: Collection<Long>): List<T> {
		val objects = ArrayList<T>()

		if (!ids.isEmpty()) {
			val authority = this.authority
			val contentUri = this.contract.getContentUri(authority)
			val projection = this.contract.getColumnNames()
			var selectionClause = BaseColumns._ID + " IN (?)"
			val selectionArgs = ArrayList<String>()
			val idsString = StringBuilder()

			idsString.append("(")

			for (id in ids) {
				idsString.append(id)
				idsString.append(',')
			}

			idsString.deleteCharAt(idsString.length - 1)
			idsString.append(")")

			selectionClause = selectionClause.replace("(?)", idsString.toString())

			val cursor = this.contentResolver.query(contentUri, projection.toTypedArray(),
					selectionClause, selectionArgs.toTypedArray(), null)

			if (cursor != null) {
				while (cursor.moveToNext()) {
					val `object` = this[cursor]

					objects.add(`object`)
				}

				cursor.close()
			}
		}

		return objects
	}

	operator fun get(projection: List<String>, orderBys: List<Pair<String, Order>>,
			groupByColumns: List<String>): List<T> {
		val selectionClause: String? = null
		val selectionArgs = emptyList<String>()
		val limit: Int? = null
		val objects =
				get(projection, selectionClause, selectionArgs, orderBys, groupByColumns, limit)

		return objects
	}

	operator fun get(id: Long): T? {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val projection = this.contract.getColumnNames()
		val selectionClause = BaseColumns._ID + " = ?"
		val selectionArgs = ArrayList<String>()

		selectionArgs.add(id.toString())

		val cursor =
				this.contentResolver.query(contentUri, projection.toTypedArray(), selectionClause,
						selectionArgs.toTypedArray(), null)
		val `object`: T?

		if (cursor == null) {
			`object` = null
		} else {
			`object` = if (cursor.moveToNext()) {
				this[cursor]
			} else {
				null
			}

			cursor.close()
		}

		return `object`
	}

	operator fun get(selectionClause: String, selectionArgs: List<String>): List<T> {
		val projection = this.contract.getColumnNames()
		val orderBys = emptyList<Pair<String, Order>>()
		val groupByColumns = emptyList<String>()
		val limit: Int? = null
		val objects =
				get(projection, selectionClause, selectionArgs, orderBys, groupByColumns, limit)

		return objects
	}

	operator fun get(selectionClause: String, selectionArgs: List<String>, limit: Int): List<T> {
		val projection = this.contract.getColumnNames()
		val orderBys = emptyList<Pair<String, Order>>()
		val groupByColumns = emptyList<String>()
		val objects =
				get(projection, selectionClause, selectionArgs, orderBys, groupByColumns, limit)

		return objects
	}

	operator fun get(selectionClause: String, selectionArgs: List<String>,
			orderBys: List<Pair<String, Order>>, limit: Int): List<T> {
		val projection = this.contract.getColumnNames()
		val groupByColumns = emptyList<String>()
		val objects =
				get(projection, selectionClause, selectionArgs, orderBys, groupByColumns, limit)

		return objects
	}

	private operator fun get(projection: List<String>, selectionClause: String?,
			selectionArgs: List<String>, orderBys: List<Pair<String, Order>>,
			groupByColumns: List<String>, limit: Int?): List<T> {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val selectionAndGroupByClauseStringBuilder = StringBuilder()

		if (selectionClause == null) {
			if (!groupByColumns.isEmpty()) {
				selectionAndGroupByClauseStringBuilder.append("1 = 1")
			}
		} else {
			selectionAndGroupByClauseStringBuilder.append(selectionClause)
		}

		if (!groupByColumns.isEmpty()) {
			selectionAndGroupByClauseStringBuilder.append(") GROUP BY (")

			for (groupByColumn in groupByColumns) {
				if (groupByColumns.indexOf(groupByColumn) != 0) {
					selectionAndGroupByClauseStringBuilder.append(", ")
				}

				selectionAndGroupByClauseStringBuilder.append(groupByColumn)
			}
		}

		val selectionAndGroupByClause = selectionAndGroupByClauseStringBuilder.toString()
		val sortOrder = StringBuilder()

		if (orderBys.isEmpty()) {
			sortOrder.append(BaseColumns._ID)
		} else {
			for (orderBy in orderBys) {
				if (sortOrder.isNotEmpty()) {
					sortOrder.append(", ")
				}

				sortOrder.append(orderBy.first)
				sortOrder.append(" ")
				sortOrder.append(orderBy.second.keyword)
			}
		}

		if (limit != null) {
			sortOrder.append(" LIMIT $limit")
		}

		val cursor = this.contentResolver.query(contentUri, projection.toTypedArray(),
				selectionAndGroupByClause, selectionArgs.toTypedArray(), sortOrder.toString())
		val objects = ArrayList<T>()

		if (cursor != null) {
			while (cursor.moveToNext()) {
				val `object` = this[cursor]

				objects.add(`object`)
			}

			cursor.close()
		}

		return objects
	}

	private operator fun get(uri: Uri): List<T> {
		val projection = this.contract.getColumnNames()
		val cursor = this.contentResolver.query(uri, projection.toTypedArray(), null, null, null)
		val objects: List<T>

		if (cursor == null) {
			objects = emptyList()
		} else {
			objects = this.getAll(cursor)

			cursor.close()
		}

		return objects
	}

	private fun getAll(cursor: Cursor): List<T> {
		val objects = ArrayList<T>()

		while (cursor.moveToNext()) {
			val `object` = this[cursor]

			objects.add(`object`)
		}

		return objects
	}

	private fun getAllDeclaredFieldsInHierarchy(clazz: Class<*>): List<Field> {
		val declaredFields = ArrayList<Field>()

		declaredFields.addAll(listOf(*clazz.declaredFields))

		var parentClazz: Class<*> = clazz.superclass

		while (parentClazz != Any::class.java) {
			val currentDeclaredFields = listOf(*parentClazz.declaredFields)

			declaredFields.addAll(currentDeclaredFields)

			parentClazz = parentClazz.superclass
		}

		return declaredFields
	}

	/**
	 * Returns the number of [DatabaseObject]s that are saved that satisfy the given
	 * selection criteria.
	 *
	 * @param selectionClause The selection clause to use to narrow down the [DatabaseObject]s
	 * to include in the count.
	 * @param selectionArgs The values to replace the placeholders with in the selection clause.
	 * @return The number of [DatabaseObject]s that are saved that satisfy the given selection
	 * criteria.
	 */
	fun getCount(selectionClause: String?, selectionArgs: List<String>): Int {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val projection = arrayOf("count(*)")
		val cursor = this.contentResolver.query(contentUri, projection, selectionClause,
				selectionArgs.toTypedArray(), null)
		val cursorCount = cursor.count
		val count = if (cursorCount == 0) {
			0
		} else {
			cursor.moveToFirst()

			cursor.getInt(0)
		}

		cursor.close()

		return count
	}

	/**
	 * Creates a new object of type [T] with all the same fields as an existing object of
	 * type [T], but with a new ID.
	 *
	 * @param oldObject The existing object to pull fields from.
	 * @param newId The ID of the newly created object.
	 * @return A new object with all the same fields as `oldObject`, but with the new ID.
	 */
	protected fun merge(oldObject: T, newId: Long): T {
		val newObject = this.createNewEmptyInstance()

		this.populate(oldObject, newObject)

		val idFieldName = "id"

		try {
			val declaredField = DatabaseObject::class.java.getDeclaredField(idFieldName)

			declaredField.isAccessible = true
			declaredField.set(newObject, newId)
		} catch (illegalAccessException: IllegalAccessException) {
			throw IllegalStateException("Unable to access 'id' field in 'DatabaseObject' class.",
					illegalAccessException)
		} catch (noSuchFieldException: NoSuchFieldException) {
			throw IllegalStateException("Unable to find 'id' field in 'DatabaseObject' " + "class.",
					noSuchFieldException)
		}

		return newObject
	}

	private fun performSave(`object`: T): T {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val contentValues = this.generateContentValues(`object`)
		val uri = this.contentResolver.insert(contentUri, contentValues)
		val idString = uri.lastPathSegment
		val id = java.lang.Long.parseLong(idString)
		val mergedObject = merge(`object`, id)

		return mergedObject
	}

	/**
	 * Updates the stored data related to the given [DatabaseObject] with the data in that
	 * [DatabaseObject].
	 *
	 * @param object The [DatabaseObject] to use in the update.
	 * @return The updated [DatabaseObject] from local storage or null if no update was done.
	 * @throws IllegalArgumentException The given [DatabaseObject] is managed internally and
	 * it is out of sync with local storage.
	 * @throws IllegalStateException The given [DatabaseObject] has already been deleted from
	 * local storage.
	 */
	@Throws(IllegalArgumentException::class, IllegalStateException::class)
	private fun performUpdate(`object`: T): T? {
		val authority = this.authority
		val id = `object`.id!!
		val whereClauseBuilder = StringBuilder()
		val whereArgs = ArrayList<String>()

		whereClauseBuilder.append(BaseColumns._ID + " = ?")
		whereArgs.add(id.toString())

		val versionManagedExternally = `object`.isVersionManagedExternally

		if (versionManagedExternally) {
			whereClauseBuilder.append(" AND " + Contract.Columns.VERSION + " < ?")

			whereArgs.add(`object`.version.toString())
		} else {
			val savedObject = this[id]

			if (savedObject == null) {
				throw IllegalStateException("Attempting to update a deleted object with: $`object`")
			} else {
				val savedObjectVersion = savedObject.version!!
				val newObjectVersion = `object`.version!!

				if (newObjectVersion < savedObjectVersion) {
					throw IllegalArgumentException(
							"Attempting to update a stale object.  The stored version of the object is '$savedObjectVersion' while the passed in object has a version of '$newObjectVersion'.")
				}

				whereClauseBuilder.append(" AND " + Contract.Columns.VERSION + " = ?")

				whereArgs.add(savedObjectVersion.toString())

				`object`.version = newObjectVersion + 1
			}
		}

		val contentUri = this.contract.getContentUri(authority, id)
		val contentValues = this.generateContentValues(`object`)
		val whereClause = whereClauseBuilder.toString()
		val i = this.contentResolver.update(contentUri, contentValues, whereClause,
				whereArgs.toTypedArray())
		val savedObject = if (i == 1) {
			`object`
		} else {
			null
		}

		return savedObject
	}

	private fun populate(databaseObject: T, cursor: Cursor?) {
		val declaredFields = this.getAllDeclaredFieldsInHierarchy(this.parameterClass)

		for (declaredField in declaredFields) {
			val column = declaredField.getAnnotation(Column::class.java)

			if (column != null) {
				val type = declaredField.type
				val isEnumConstant = type.isEnum
				val columnName = column.name
				val index = cursor!!.getColumnIndex(columnName)
				var value: Any? = null

				if (isEnumConstant) {
					val enumValueName = cursor.getString(index)

					if (enumValueName.isEmpty()) {
						value = null
					} else {
						@Suppress("UNCHECKED_CAST") val enumConstants =
								type.enumConstants as Array<Enum<*>>

						for (enumConstant in enumConstants) {
							if (enumConstant.name == enumValueName) {
								value = enumConstant

								break
							}
						}

						if (value == null) {
							val errorMessage =
									String.format("Failed to parse enum value of \"%1\$s\".",
											enumValueName)

							throw IllegalArgumentException(errorMessage)
						}
					}
				} else {
					value = when (type) {
						Boolean::class.java, java.lang.Boolean::class.java -> cursor.getShort(
								index) != 0.toShort()
						Date::class.java -> Date(cursor.getLong(index))
						Int::class.java, java.lang.Integer::class.java -> cursor.getInt(index)
						Long::class.java, java.lang.Long::class.java -> cursor.getLong(index)
						String::class.java, java.lang.String::class.java -> cursor.getString(index)
						else -> throw IllegalArgumentException("Found unknown column type '$type'.")
					}
				}

				try {
					declaredField.isAccessible = true
					declaredField.set(databaseObject, value)
				} catch (illegalAccessException: IllegalAccessException) {
					val name = declaredField.name

					throw IllegalStateException(
							String.format("Unable to access '$1%s' field in '$2%s' class.", name,
									this.parameterClass.simpleName), illegalAccessException)

				}
			}
		}
	}

	private fun populate(sourceDatabaseObject: T, destinationDatabaseObject: T) {
		val declaredFields = this.getAllDeclaredFieldsInHierarchy(this.parameterClass)

		for (declaredField in declaredFields) {
			try {
				declaredField.isAccessible = true

				val value = declaredField.get(sourceDatabaseObject)

				declaredField.set(destinationDatabaseObject, value)
			} catch (illegalAccessException: IllegalAccessException) {
				val name = declaredField.name

				throw IllegalStateException(
						String.format("Unable to access '$1%s' field in '$2%s' class.", name,
								this.parameterClass.simpleName), illegalAccessException)
			}
		}
	}

	/**
	 * Creates an "insert" [ContentProviderOperation] for each of the given objects.
	 *
	 * @param objectsToAdd The objects to create [ContentProviderOperation] for.
	 * @return The created [ContentProviderOperation]s.
	 */
	private fun processObjectsToAdd(objectsToAdd: Collection<T>): List<ContentProviderOperation> {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val contentProviderOperations = ArrayList<ContentProviderOperation>()

		for (objectToAdd in objectsToAdd) {
			val builder = ContentProviderOperation.newInsert(contentUri)
			val contentValues = this.generateContentValues(objectToAdd)

			builder.withValues(contentValues)

			val objectToAddContentProviderOperation = builder.build()

			contentProviderOperations.add(objectToAddContentProviderOperation)
		}

		return contentProviderOperations
	}

	/**
	 * Creates a "delete" [ContentProviderOperation] for each of the given objects.
	 *
	 * @param objectsToDelete The objects to create [ContentProviderOperation] for.
	 * @return The created [ContentProviderOperation]s.
	 */
	private fun processObjectsToDelete(
			objectsToDelete: Collection<T>): List<ContentProviderOperation> {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val contentProviderOperations = ArrayList<ContentProviderOperation>()

		for (objectToDelete in objectsToDelete) {
			val id = objectToDelete.id!!
			val modifiedObjectUri = ContentUris.withAppendedId(contentUri, id)
			val builder = ContentProviderOperation.newDelete(modifiedObjectUri)
			val selectionClause = BaseColumns._ID + " = ?"
			val selectionArgs = ArrayList<String>()

			selectionArgs.add(id.toString())

			builder.withSelection(selectionClause, selectionArgs.toTypedArray())

			val objectToDeleteContentProviderOperation = builder.build()

			contentProviderOperations.add(objectToDeleteContentProviderOperation)
		}

		return contentProviderOperations
	}

	/**
	 * Creates an "update" [ContentProviderOperation] for each of the given objects.
	 *
	 * @param objectsToUpdate The objects to create [ContentProviderOperation] for.
	 * @return The created [ContentProviderOperation]s.
	 */
	private fun processObjectsToUpdate(
			objectsToUpdate: Collection<T>): List<ContentProviderOperation> {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val contentProviderOperations = ArrayList<ContentProviderOperation>()

		for (objectToUpdate in objectsToUpdate) {
			val id = objectToUpdate.id!!
			val modifiedObjectUri = ContentUris.withAppendedId(contentUri, id)
			val builder = ContentProviderOperation.newUpdate(modifiedObjectUri)
			val contentValues = this.generateContentValues(objectToUpdate)
			val selectionClause = BaseColumns._ID + " = ?"
			val selectionArgs = ArrayList<String>()

			selectionArgs.add(id.toString())

			builder.withSelection(selectionClause, selectionArgs.toTypedArray())
			builder.withValues(contentValues)

			val objectToUpdateContentProviderOperation = builder.build()

			contentProviderOperations.add(objectToUpdateContentProviderOperation)
		}

		return contentProviderOperations
	}

	/**
	 * Replaces the existing collection of saved database objects with the given collection.
	 *
	 * @param replacementObjects The newer collection of database objects that should overwrite the
	 * existing collection.
	 * @return The latest version of the objects that have been saved or updated.
	 * @throws StorageModificationException One of the replacement operations (either add, update,
	 * or delete) failed.
	 */
	@Throws(StorageModificationException::class)
	fun replace(replacementObjects: Collection<T>): List<T> {
		val selectionClause: String? = null
		val selectionArgs = emptyList<String>()
		val objectsChanged = replace(replacementObjects, selectionClause, selectionArgs)

		return objectsChanged
	}

	/**
	 * Replaces the existing collection of saved database objects with the given collection.
	 *
	 * @param replacementObjects The newer collection of database objects that should overwrite the
	 * existing collection.
	 * @param selectionClause A filter declaring which rows to replace, formatted as an SQL WHERE
	 * clause (excluding the WHERE itself).
	 * @param selectionArgs You may include ?s in the selection clause, which will be replaced by
	 * the values from selectionArgs, in the order that they appear in the selection. The values
	 * will be bound as Strings.
	 * @return The latest version of the objects that have been saved or updated.
	 * @throws StorageModificationException One of the replacement operations (either add, update,
	 * or delete) failed.
	 */
	@Throws(StorageModificationException::class)
	fun replace(replacementObjects: Collection<T>, selectionClause: String?,
			selectionArgs: List<String>): List<T> {
		val existingObjects = if (selectionClause == null) {
			this.get()
		} else {
			this[selectionClause, selectionArgs]
		}

		val objectsToAddUpdateAndDeleteTriple = this.categorize(existingObjects, replacementObjects)
		val objectsToAdd = objectsToAddUpdateAndDeleteTriple.left
		val objectsToUpdate = objectsToAddUpdateAndDeleteTriple.middle
		val objectsToDelete = objectsToAddUpdateAndDeleteTriple.right
		val objectsChanged = apply(objectsToAdd, objectsToUpdate, objectsToDelete)

		return objectsChanged
	}

	@Throws(StorageModificationException::class)
	fun save(objects: Collection<T>): List<T> {
		val existingObjects = this.get()
		val objectsToAddUpdateAndDeleteTriple = this.categorize(existingObjects, objects)
		val objectsToAdd = objectsToAddUpdateAndDeleteTriple.left
		val objectsToUpdate = objectsToAddUpdateAndDeleteTriple.middle
		val savedObjects = apply(objectsToAdd, objectsToUpdate)

		return savedObjects
	}

	/**
	 * Saves/updates the given [DatabaseObject] to/in local storage.  If the version of the
	 * [DatabaseObject] is managed internally, the save will succeed only if the given [DatabaseObject] has never been saved before or the saved [DatabaseObject] version is
	 * the same as the given [DatabaseObject] version. If the version of the [DatabaseObject] is managed externally, the save will succeed only if the given [DatabaseObject] has never been saved before or the saved [DatabaseObject] version is
	 * older than the given [DatabaseObject] version.
	 *
	 * @param object The [DatabaseObject] to save/update.
	 * @return The newly saved object, or `null` if no save was done.
	 * @throws IllegalArgumentException The given [DatabaseObject] is managed internally and
	 * it is out of sync with local storage.
	 */
	fun save(`object`: T): T? {
		val idManagedExternally = `object`.isIdManagedExternally
		val savedObject: T?

		if (idManagedExternally) {
			val id = `object`.id!!
			val existingObject = this[id]

			if (existingObject == null) {
				val attemptedSaveObject = try {
					this.performSave(`object`)
				} catch (sqlException: SQLException) {
					val justAddedDuplicateObject = this[id]

					if (justAddedDuplicateObject == null) {
						throw sqlException
					} else {
						this.performUpdate(`object`)
					}
				}

				savedObject = attemptedSaveObject
			} else {
				savedObject = this.performUpdate(`object`)
			}
		} else {
			val id = `object`.id

			savedObject = if (id == null) {
				this.performSave(`object`)
			} else {
				this.performUpdate(`object`)
			}
		}

		return savedObject
	}

	/**
	 * Updates all objects with the given field values.
	 *
	 * @param contentValues The new field values. The key is the column name for the field. A null
	 * value will remove an existing field value.
	 * @return The number of objects updated.
	 */
	protected fun update(contentValues: ContentValues): Int {
		val whereClause: String? = null
		val whereArgs = emptyList<String>()
		val numberOfUpdatedObjects = update(contentValues, whereClause, whereArgs)

		return numberOfUpdatedObjects
	}

	/**
	 * Updates all objects that match the where clause with the given field values.
	 *
	 * @param contentValues The new field values. The key is the column name for the field. A null
	 * value will remove an existing field value.
	 * @param whereClause A filter to apply to rows before updating, formatted as an SQL WHERE
	 * clause (excluding the WHERE itself).
	 * @param whereArgs The values with which to replace the question marks in the where clause.
	 * @return The number of objects updated.
	 */
	protected fun update(contentValues: ContentValues, whereClause: String?,
			whereArgs: List<String>): Int {
		val authority = this.authority
		val contentUri = this.contract.getContentUri(authority)
		val numberOfUpdatedObjects = contentResolver.update(contentUri, contentValues, whereClause,
				whereArgs.toTypedArray())

		return numberOfUpdatedObjects
	}
}