package com.wit.databaselibrary.contentprovider.contract

import android.content.ContentResolver
import android.content.UriMatcher
import android.net.Uri
import android.provider.BaseColumns
import com.wit.databaselibrary.annotation.Column
import com.wit.databaselibrary.annotation.Table
import com.wit.databaselibrary.model.DatabaseObject
import java.lang.reflect.Field
import java.util.*

abstract class Contract<T : DatabaseObject>(private val databaseObjectClass: Class<T>) {
	companion object {
		private fun getAllFields(clazz: Class<*>): List<Field> {
			val fields = ArrayList(Arrays.asList(*clazz.declaredFields))
			val superclass = clazz.superclass

			if (superclass != Any::class.java) {
				val newFields = Contract.getAllFields(superclass)

				fields.addAll(newFields)
			}

			return fields
		}
	}

	private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)
	private val objectCode = 1
	private val objectIdCode = 2
	private val projectionMap = HashMap<String, String>()
	private val columnNames = ArrayList<String>()

	/**
	 * Returns the SQL string needed to create a database table for the [ ] associated with this [Contract].
	 *
	 * @return The SQL string needed to create a database table for the [ ] associated with this [Contract].
	 */
	val createTableSqlString: String

	private var uriMatcherPrepared = false

	val contentType: String
		get() {
			val tableName = this.tableName
			val contentType = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.wit." + tableName

			return contentType
		}

	val contentItemType: String
		get() {
			val tableName = this.tableName
			val contentItemType = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.wit." + tableName

			return contentItemType
		}

	val tableName: String
		get() {
			val tableAnnotation = this.databaseObjectClass.getAnnotation(Table::class.java)
			val tableName = tableAnnotation.tableName

			return tableName
		}

	abstract class Columns : BaseColumns {
		companion object {
			/**
			 * The version of an object.
			 */
			const val VERSION = "version"
		}
	}

	init {
		this.setupProjectionMap()
		this.setupColumnNameList()

		this.createTableSqlString = this.generateCreateTableSqlString()
	}

	fun addSelectionById(uri: Uri, existingSelection: String): String {
		val selectionByIdString =
				existingSelection + BaseColumns._ID + " = " + "" + uri.lastPathSegment

		return selectionByIdString
	}

	/**
	 * Generates the SQL string needed to create a database table for the
	 * [ ] associated with this [Contract].
	 *
	 * @return The SQL string needed to create a database table for the [ ] associated with this [Contract].
	 */
	private fun generateCreateTableSqlString(): String {
		val createSqlStringBuilder = StringBuilder()

		createSqlStringBuilder.append("CREATE TABLE")

		val tableAnnotation = this.databaseObjectClass.getAnnotation(Table::class.java)
		val tableName = tableAnnotation.tableName

		createSqlStringBuilder.append(" ")
		createSqlStringBuilder.append(tableName)
		createSqlStringBuilder.append(" ")
		createSqlStringBuilder.append("(")
		createSqlStringBuilder.append(" ")

		val declaredFields = Contract.getAllFields(this.databaseObjectClass)

		for (declaredField in declaredFields) {
			val columnAnnotation = declaredField.getAnnotation(Column::class.java)

			if (columnAnnotation != null) {
				val name = columnAnnotation.name
				val type = declaredField.type

				createSqlStringBuilder.append(name)
				createSqlStringBuilder.append(" ")

				val columnTypeString: String

				columnTypeString = if (type.isEnum) {
					"TEXT"
				} else {
					when (type) {
						Boolean::class.java, java.lang.Boolean::class.java -> "INTEGER"
						Date::class.java -> "INTEGER"
						Int::class.java, java.lang.Integer::class.java -> "INTEGER"
						Long::class.java, java.lang.Long::class.java -> "INTEGER"
						String::class.java, java.lang.String::class.java -> "TEXT"
						else -> throw IllegalArgumentException(
								"\"$type\" is an unrecognized field type.")
					}
				}

				createSqlStringBuilder.append(columnTypeString)

				if (name == BaseColumns._ID) {
					createSqlStringBuilder.append(" ")
					createSqlStringBuilder.append("PRIMARY KEY")
				}

				createSqlStringBuilder.append(",")
				createSqlStringBuilder.append(" ")
			}
		}

		createSqlStringBuilder.replace(createSqlStringBuilder.length - 2,
				createSqlStringBuilder.length, "")

		createSqlStringBuilder.append(" ")
		createSqlStringBuilder.append(")")

		val createTableSqlString = createSqlStringBuilder.toString()

		return createTableSqlString
	}

	fun getColumnNames(): List<String> {
		val columnNames = this.columnNames

		return columnNames
	}

	fun getContentUri(authority: String): Uri {
		val contentUriString = this.getContentUriString(authority)
		val contentUri = Uri.parse(contentUriString)

		return contentUri
	}

	fun getContentUri(authority: String, id: Long): Uri {
		val contentUriString = this.getContentUriString(authority)
		val contentUri = Uri.parse("$contentUriString/$id")

		return contentUri
	}

	/**
	 * Returns the root content URI as a [String].
	 *
	 * @param authority The authority string to use in the root content URI.
	 * @return The root content URI as a [String].
	 */
	private fun getContentUriString(authority: String): String {
		val tableName = this.tableName
		val contentUriString = "content://$authority/$tableName"

		return contentUriString
	}

	/**
	 * Extracts the ID from the given [Uri].
	 *
	 * @param uri The [Uri] with the ID that should be extracted.
	 * @return The ID from the given [Uri].
	 * @throws IllegalArgumentException The given [Uri] did not have
	 * exactly 2 path segments, one for the root object and one for the
	 * specific ID.
	 */
	@Throws(IllegalArgumentException::class)
	fun getId(uri: Uri): Long {
		val pathSegments = uri.pathSegments
		val numberOfPathSegments = pathSegments.size
		val id: Long

		if (numberOfPathSegments == 2) {
			id = java.lang.Long.valueOf(pathSegments[1])
		} else {
			throw IllegalArgumentException("Unable to parse ID from URI '" + uri.path + "'.")
		}

		return id
	}

	fun getProjectionMap(): Map<String, String> {
		return this.projectionMap
	}

	/**
	 * Returns whether the given [Uri] contains an ID.
	 *
	 * @param uri The [Uri] to check for the existence of an ID.
	 * @return Whether the given [Uri] contains an ID.
	 */
	fun hasId(uri: Uri): Boolean {
		val pathSegments = uri.pathSegments
		val numberOfPathSegments = pathSegments.size
		val hasId = numberOfPathSegments == 2

		return hasId
	}

	private fun setupProjectionMap() {
		val declaredFields = Contract.getAllFields(this.databaseObjectClass)

		for (declaredField in declaredFields) {
			val columnAnnotation = declaredField.getAnnotation(Column::class.java)

			if (columnAnnotation != null) {
				val columnName = columnAnnotation.name

				this.projectionMap[columnName] = columnName
			}
		}
	}

	private fun setupColumnNameList() {
		this.columnNames.addAll(this.projectionMap.keys)
	}

	private fun prepareUriMatcher(authority: String) {
		val tableName = this.tableName

		this.uriMatcher.addURI(authority, tableName, this.objectCode)
		this.uriMatcher.addURI(authority, "$tableName/#", this.objectIdCode)
	}

	fun uriMatches(uri: Uri, authority: String): Boolean {
		return this.uriMatches(uri, true, true, authority)
	}

	@Synchronized
	private fun uriMatches(uri: Uri, matchOnObjectCode: Boolean, matchOnObjectIdCode: Boolean,
			authority: String): Boolean {
		if (!this.uriMatcherPrepared) {
			this.prepareUriMatcher(authority)
			this.uriMatcherPrepared = true
		}

		val matchResult = this.uriMatcher.match(uri)
		val match =
				matchOnObjectCode && matchResult == this.objectCode || matchOnObjectIdCode && matchResult == this.objectIdCode

		return match
	}

	fun uriMatchesObject(uri: Uri, authority: String): Boolean {
		return this.uriMatches(uri, true, false, authority)
	}

	fun uriMatchesObjectId(uri: Uri, authority: String): Boolean {
		return this.uriMatches(uri, false, true, authority)
	}
}