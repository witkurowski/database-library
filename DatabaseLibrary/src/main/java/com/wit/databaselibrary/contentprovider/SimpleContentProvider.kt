package com.wit.databaselibrary.contentprovider

import android.content.ContentProvider
import android.content.ContentProviderOperation
import android.content.ContentProviderResult
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.OperationApplicationException
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import android.provider.BaseColumns
import android.support.annotation.CallSuper
import com.wit.databaselibrary.contentprovider.contract.Contract
import java.util.*

abstract class SimpleContentProvider(private val contracts: Set<Contract<*>>) : ContentProvider() {
	private var simpleDatabaseHelper: SimpleDatabaseHelper? = null

	protected abstract val authority: String

	private fun adjustSelection(uri: Uri, selection: String, authority: String): String {
		var newSelection = selection
		for (contract in this.contracts) {
			if (contract.uriMatchesObjectId(uri, authority)) {
				newSelection = contract.addSelectionById(uri, selection)

				break
			}
		}

		return newSelection
	}

	@Throws(OperationApplicationException::class)
	override fun applyBatch(
			contentProviderOperations: ArrayList<ContentProviderOperation>): Array<ContentProviderResult> {
		val sqLiteDatabase = this.simpleDatabaseHelper!!.writableDatabase

		sqLiteDatabase.beginTransaction()

		val contentProviderResults = ArrayList<ContentProviderResult>()

		try {
			for (contentProviderOperation in contentProviderOperations) {
				val contentProviderResult = contentProviderOperation.apply(this, null, 0)

				contentProviderResults.add(contentProviderResult)
			}

			sqLiteDatabase.setTransactionSuccessful()
		} finally {
			sqLiteDatabase.endTransaction()
		}

		return contentProviderResults.toTypedArray()
	}

	protected abstract fun createDatabaseHelper(): SimpleDatabaseHelper

	override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
		val authority = this.authority
		val tableName = this.getTableName(uri, authority)
		val sqLiteDatabase = this.simpleDatabaseHelper!!.writableDatabase

		return sqLiteDatabase.delete(tableName, selection, selectionArgs)
	}

	/**
	 * Returns the [Contract] that successfully matches the given [Uri] by object.
	 *
	 * @param uri The [Uri] to try to match.
	 * @return The [Contract] that successfully matches the given [Uri] by object.
	 * @throws IllegalArgumentException The given [Uri] did not match any [Contract] by
	 * object.
	 */
	@Throws(IllegalArgumentException::class)
	private fun getContractByMatchingObject(uri: Uri): Contract<*> {
		val authority = this.authority
		var contract: Contract<*>? = null

		for (currentContract in this.contracts) {
			if (currentContract.uriMatchesObject(uri, authority)) {
				contract = currentContract
			}
		}

		if (contract == null) {
			throw IllegalArgumentException(
					"Unknown URI, \"" + uri + "\". Please ensure that it has been added to the list of Contracts passed into the " + SimpleContentProvider::class.java.simpleName + " class.")
		}

		return contract
	}

	private fun getTableName(uri: Uri, authority: String): String {
		var tableName: String? = null

		for (contract in this.contracts) {
			if (contract.uriMatches(uri, authority)) {
				tableName = contract.tableName

				break
			}
		}

		if (tableName == null) {
			throw IllegalArgumentException(
					"Unknown URI, \"" + uri + "\". Are you sure you added it to the list of Contracts passed into the " + SimpleContentProvider::class.java.simpleName + " class?")
		}

		return tableName
	}

	override fun getType(uri: Uri): String {
		val authority = this.authority
		var contentType: String? = null

		for (contract in this.contracts) {
			if (contract.uriMatchesObject(uri, authority)) {
				contentType = contract.contentType

				break
			} else if (contract.uriMatchesObjectId(uri, authority)) {
				contentType = contract.contentItemType

				break
			}
		}

		if (contentType == null) {
			throw IllegalArgumentException(
					"Unknown URI, \"" + uri + "\". Are you sure you added it to the list of Contracts passed into the " + SimpleContentProvider::class.java.simpleName + " class?")
		}

		return contentType
	}

	override fun insert(uri: Uri, contentValues: ContentValues?): Uri {
		val newContentValues = contentValues ?: ContentValues()
		val contract = this.getContractByMatchingObject(uri)
		val sqLiteDatabase = this.simpleDatabaseHelper!!.writableDatabase
		val tableName = contract.tableName
		val nullColumnHack = if (newContentValues.size() == 0) {
			BaseColumns._ID
		} else {
			null
		}

		val rowId = sqLiteDatabase.insertOrThrow(tableName, nullColumnHack, newContentValues)

		if (rowId > 0) {
			val contentUriWithAppendedId = ContentUris.withAppendedId(uri, rowId)
			val context = this.context
			val contentResolver = context.contentResolver

			contentResolver.notifyChange(contentUriWithAppendedId, null)

			return contentUriWithAppendedId
		} else {
			throw SQLException("Failed to insert row into $uri")
		}
	}

	override fun onCreate(): Boolean {
		this.simpleDatabaseHelper = this.createDatabaseHelper()

		return false
	}

	override fun query(uri: Uri, projection: Array<String>?, selection: String?,
			selectionArgs: Array<String>?, sortOrder: String?): Cursor {
		val authority = this.authority
		var projectionMap: Map<String, String>? = null

		for (contract in this.contracts) {
			if (contract.uriMatches(uri, authority)) {
				projectionMap = contract.getProjectionMap()
			}
		}

		if (projectionMap == null) {
			throw IllegalArgumentException(
					"Unknown URI, \"" + uri + "\". Are you sure you added it to the list of Contracts passed into the " + SimpleContentProvider::class.java.simpleName + " class?")
		}

		val newSelection = if (selection == null) {
			this.adjustSelection(uri, "", authority)
		} else {
			this.adjustSelection(uri, selection, authority)
		}

		val sqLiteQueryBuilder = SQLiteQueryBuilder()
		val tableName = this.getTableName(uri, authority)

		sqLiteQueryBuilder.tables = tableName
		sqLiteQueryBuilder.setProjectionMap(projectionMap)

		val sqLiteDatabase = this.simpleDatabaseHelper!!.readableDatabase
		val cursor =
				sqLiteQueryBuilder.query(sqLiteDatabase, projection, newSelection, selectionArgs,
						null, null, sortOrder)

		cursor.setNotificationUri(this.context.contentResolver, uri)

		return cursor
	}

	override fun update(uri: Uri, contentValues: ContentValues?, selection: String?,
			selectionArgs: Array<String>?): Int {
		val sqLiteDatabase = this.simpleDatabaseHelper!!.writableDatabase
		val authority = this.authority
		val tableName = this.getTableName(uri, authority)

		return sqLiteDatabase.update(tableName, contentValues, selection, selectionArgs)
	}

	abstract class SimpleDatabaseHelper(context: Context, name: String, version: Int,
			private val contracts: Set<Contract<*>>) :
			SQLiteOpenHelper(context, name, null, version) {
		@CallSuper
		override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
			for (contract in this.contracts) {
				sqLiteDatabase.execSQL(contract.createTableSqlString)
			}
		}

		override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
		}
	}
}