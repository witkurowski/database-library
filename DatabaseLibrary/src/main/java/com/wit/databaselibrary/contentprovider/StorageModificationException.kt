package com.wit.databaselibrary.contentprovider

/**
 * Used when an attempt to add, update, or delete data from storage has failed.
 */
class StorageModificationException(detailedMessage: String, throwable: Throwable) :
		Exception(detailedMessage, throwable)